api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7.28"

projects[guusvandewal][type] = profile
projects[guusvandewal][download][type] = git
projects[guusvandewal][download][branch] = development
projects[guusvandewal][download][url] = git@bitbucket.org:guusvandewal/base_profile.git
