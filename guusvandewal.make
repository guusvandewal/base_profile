api = 2
core = 7.x

projects[drupal][version] = "7"

; Dependencies =================================================================

projects[ctools][type] = module
projects[ctools][subdir] = contrib
projects[ctools][version] = 1.4
projects[ctools][download][type] = git
projects[ctools][download][url] = http://git.drupal.org/project/ctools.git
projects[ctools][download][branch] = 7.x-1.x

projects[devel][type] = module
projects[devel][subdir] = contrib
projects[devel][version] = 1.5
projects[devel][download][type] = git
projects[devel][download][url] = http://git.drupal.org/project/devel.git
projects[devel][download][branch] = 7.x-1.x

projects[libraries][type] = module
projects[libraries][subdir] = contrib
projects[libraries][version] = 2.2
projects[libraries][download][type] = git
projects[libraries][download][url] = http://git.drupal.org/project/libraries.git
projects[libraries][download][branch] = 7.x-2.x


; projects[toolbar_admin_menu][type] = module
; projects[toolbar_admin_menu][subdir] = contrib
; projects[toolbar_admin_menu][version] = 1.0
; projects[toolbar_admin_menu][download][type] = git
; projects[toolbar_admin_menu][download][url] = http://git.drupalcode.org/project/toolbar_admin_menu.git
; projects[toolbar_admin_menu][download][branch] = 7.x-1.x

projects[wysiwyg][type] = module
projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.2
projects[wysiwyg][download][type] = git
projects[wysiwyg][download][url] = http://git.drupal.org/project/wysiwyg.git
projects[wysiwyg][download][branch] = 7.x-2.x


projects[token][version]        = "1.5"
projects[token][subdir]         = "contrib"

projects[scald][version]        = "1.1"
projects[scald][subdir]         = "contrib"

projects[scald_file][version]           = "1.0-beta3"
projects[scald_file][subdir]            = "contrib"

projects[webform][version]           = "3.20"
projects[webform][subdir]            = "contrib"

projects[field_group][version]       = "1.3"
projects[field_group][subdir]        = "contrib"

projects[pathauto][version]        = "1.2"
projects[pathauto][subdir]         = "contrib"

projects[views][version]           = "3.8"
projects[views][subdir]            = "contrib"

projects[link_css][version]       = "1.0"
projects[link_css][subdir]        = "contrib"

projects[features][version]        = "2.0"
projects[features][subdir]         = "contrib"

projects[strongarm][version]       = "2.0"
projects[strongarm][subdir]        = "contrib"

projects[panels][subdir] = "contrib"
projects[panels][version] = "3.4"

projects[panelizer][subdir] = "contrib"
projects[panelizer][version] = "3.1"

projects[local_tasks_blocks][version] = "2.2"
projects[local_tasks_blocks][subdir] = "contrib"

projects[entity][version]          = "1.5"
projects[entity][subdir]           = "contrib"

projects[paragraphs][subdir] = "contrib"

projects[module_filter][version]     = "2.0-alpha2"
projects[module_filter][subdir]      = "contrib"

projects[google_analytics][version]     = "1.4"
projects[google_analytics][subdir]      = "contrib"

projects[xmlsitemap][version]           = "2.0"
projects[xmlsitemap][subdir]            = "contrib"

projects[jquery_update][version]        = "2.4"
projects[jquery_update][subdir]         = "contrib"

projects[backup_migrate][version]       = "3.0"
projects[backup_migrate][subdir]        = "contrib"

projects[diff][version]                 = "3.2"
projects[diff][subdir]                  = "contrib"

; Themes =======================================================================
projects[tao][type] = theme
projects[tao][version] = 3.1
projects[tao][download][type] = git
projects[tao][download][url] = http://git.drupal.org/project/tao.git
projects[tao][download][branch] = 7.x-3.1

projects[rubik][type] = theme
projects[rubik][version] = 4.1
projects[rubik][download][type] = git
projects[rubik][download][url] = http://git.drupal.org/project/rubik.git
projects[rubik][download][branch] = 7.x-4.1

projects[guusvandewal_theme][type] = theme
projects[guusvandewal_theme][download][type] = git
projects[guusvandewal_theme][download][url] = git@bitbucket.org:guusvandewal/base_theme.git
projects[guusvandewal_theme][download][branch] = master


; Libraries ====================================================================
; libraries[tinymce][download][type] = git
; libraries[tinymce][download][url] = https://github.com/tinymce/tinymce.git
libraries[tinymce][type] = library
libraries[tinymce][download][type] = get
libraries[tinymce][download][url] = http://github.com/downloads/tinymce/tinymce/tinymce_3.5.6.zip
libraries[tinymce][download][branch] = 3.4.x
